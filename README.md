# Geekstuff.dev / Devcontainers / Features / Gcloud CLI

This devcontainer feature installs Gcloud CLI, and its autocomplete.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/gcloud-cli": {
            "project": "my-gcp-project",
            "region": "gcp-region"
        }
    }
}
```

You can use any debian, ubuntu or alpine image as the base.

This feature requires the basics feature since it needs the non-root user.

`project` and `region` options are optional and if set will translate to env vars
`GCLOUD_PROJECT` and `GCLOUD_REGION` set to those values in the devcontainer.

The above will pull use latest version of that feature, otherwise with **an example**
`v1.2.3` tag in this project source code, you would be able to use tags such as:

- `example.registry/some/path/feature:1`
- `example.registry/some/path/feature:1.2`
- `example.registry/some/path/feature:1.2.3`
- `example.registry/some/path/feature:latest`

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/gcloud-cli/-/tags).
